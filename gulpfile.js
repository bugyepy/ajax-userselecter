/* global require */

var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');

//  test command
//  sass to css
gulp.task('sass', function () {
  gulp.src('src/sass/*.scss')
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'expanded'
    }))
    .pipe(gulp.dest('dist/css'));
});

//  html copy
gulp.task('html', function () {
  gulp.src('src/*.html')
    .pipe(gulp.dest('dist'));
});

// js copy
gulp.task('js', function () {
  gulp.src('src/*/*.js')
    .pipe(gulp.dest('dist'));
});

// jpg copy
gulp.task('jpg', function () {
  gulp.src('src/*/*.jpg')
    .pipe(gulp.dest('dist'));
});

// reloader
gulp.task('bs-reload', function () {
  browserSync.reload();
});

//  watcher
gulp.task('watcher', ['browser-sync'], function () {
  gulp.watch('src/*.html', ['html', 'bs-reload']);
  gulp.watch('src/*/*.scss', ['sass', 'bs-reload']);
  gulp.watch('src/*/*.js', ['js', 'bs-reload']);
});

//  browser-sync
gulp.task('browser-sync', function () {
  browserSync({
    server: {
      baseDir: 'dist',
      index: 'index.html'
    }
  });
});

gulp.task('default', ['jpg', 'js', 'html', 'sass', 'watcher', 'browser-sync']);
gulp.task('build', ['jpg', 'js', 'html', 'sass']);
