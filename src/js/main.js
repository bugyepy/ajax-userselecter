/* global $ */
// 現在のページ数
var i = 1;
// カードの展開数
var card = 6;

// 初期ロード
$(document).on('load', function () {
  // ロードテキスト展開
  $('.js-loading').addClass('loading-text');
});
$.ajax({
  url: 'https://randomuser.me/api?page=' + i + '&results=' + card + '&seed=vt2',
  dataType: 'json',
  success: function (data) {
    data.results.forEach (function (user, index) {
      // 結合用変数宣言
      var dom = ''
      // 枠
      + '<div class="profile js-modal" id="js-profile-' + index + '">'
      + '\n<img class="profile__photo" src="' + user.picture.large + '" alt="">'
      + '\n<div class="profile__description" id="js-description-' + index + '">'
      // テキスト部
      // 名前
      + '\n<p class="profile__text js-loading js-text">' + user.name.title + ' ' + user.name.first + ' ' + user.name.last + '</p>'
      // 居住地
      + '\n<p class="profile__text-local js-text-local">' + user.location.state + ' state, ' + user.location.city + ', ' + user.location.street + ', ' + user.location.postcode + ', ' + '</p>'
      // 誕生日
      + '\n<p class="profile__text-birth js-text-birth">' + user.dob.date + ' , age ' + user.dob.age + ',' + '</p>'
      // DIVタグ閉じ
      + '\n</div>'+ '\n</div>';
      // Append適用
      $('#js-profile-box').append(dom);
    });
    // MOREボタン展開
    var more = ''
    + '<div class="morebutton-dammy"></div>'
    + '\n<div class="morebutton-left" id="js-more-left"></div>'
    + '\n<div class="morebutton" id="js-more">\n<p class="morebutton__text">MORE</p></div>'
    + '\n<div class="morebutton-right" id="js-more-right"></div>';
    // Append適用
    $('#js-profile-box').append(more);
    // ロードテキスト削除
    $('.js-loading').removeClass('loading-text');
  },
});

// モーダル展開
$(function () {
  $('#js-profile-box').on('click', '.js-modal', function () {
    var profIndex = $(this).index('.js-modal');
    $.ajax({
      url: 'https://randomuser.me/api?page=' + i + '&results=' + card + '&seed=vt2',
      dataType: 'json',
      success: function (data) {
        // 参照先
        var user = data.results[profIndex];
        // 顔写真
        $('#js-modal-photo').attr('src', user.picture.large);
        // 名前
        $('#js-modal-name').text(user.name.first + ' ' + user.name.last);
        // 電話番号
        $('#js-modal-phone').text('Phone code: ' + user.phone);
        // 携帯電話
        $('#js-modal-cell').text('Cellphone code: ' + user.cell);
        // メールアドレス
        $('#js-modal-email').text('Email address: ' + user.email);
        // ユーザー名
        $('#js-modal-username').text('Username: ' + user.login.username);
        // パスワード
        $('#js-modal-password').text('Password: ' + user.login.password);
        // クラスをアクティブに
        $('#js-modal-box').addClass('modal-is-active');
      },
    });
  });
});

// モーダル削除
$(function () {
  $('#js-modal-box').on('click', '#js-close-point', function () {
    $('#js-modal-box').removeClass('modal-is-active');
    return false; 
  });
});

// MOREボタンと右矢印
$(function () {
  $('#js-profile-box').on('click', '#js-more, #js-more-right', function () {
    i++;
    $('.morebutton').addClass('morebutton--active');
    $('.morebutton-left').addClass('morebutton-left--active');
    $('.morebutton-right').addClass('morebutton-right--active');
    $('.profile__description').addClass('loading-more').removeClass('loading-more-done loading-more-done-reverse');
    $('.profile__photo').addClass('loading-more-photo').removeClass('loading-more-done-photo loading-more-done-reverse-photo');
    // ajax更新
    $.ajax({
      url: 'https://randomuser.me/api?page=' + i + '&results=' + card + '&seed=vt2',
      dataType: 'json',
      success: function (data) {
        data.results.forEach (function (user, index) {
          // DOM削除
          $('#js-profile-' + index).empty();
          var dom = ''
          // 顔写真とテキスト枠
          + '\n<img class="profile__photo" src="' + user.picture.large + '" alt="">'
          + '\n<div class="profile__description" id="js-description-' + index + '">'
          // テキスト部
          // 名前
          + '\n<p class="profile__text js-loading js-text">' + user.name.title + ' ' + user.name.first + ' ' + user.name.last + '</p>'
          // 居住地
          + '\n<p class="profile__text-local js-text-local">' + user.location.state + ' state, ' + user.location.city + ', ' + user.location.street + ', ' + user.location.postcode + ', ' + '</p>'
          // 誕生日
          + '\n<p class="profile__text-birth js-text-birth">' + user.dob.date + ' , age ' + user.dob.age + ',' + '</p>'
          // DIVタグ閉じ
          + '\n</div>';
          $('#js-profile-' + index).append(dom);
        });
      },
    })
      .done(function () {
        $('.profile__description').removeClass('loading-more').addClass('loading-more-done');
        $('.profile__photo').removeClass('loading-more-photo').addClass('loading-more-done-photo');
      });
    return false;
  });
});

// 左矢印
$(function () {
  $('#js-profile-box').on('click', '#js-more-left', function () {
    if (i >= 2) {
      i--;
      $('.profile__description').removeClass('loading-more--done loading-more-done-reverse').addClass('loading-more-reverse');
      $('.profile__photo').removeClass('loading-more-done-photo loading-more-done-reverse-photo').addClass('loading-more-photo-reverse');
      // 左なので逆転させている
      // ajax更新
      $.ajax({
        url: 'https://randomuser.me/api?page=' + i + '&results=' + card + '&seed=vt2',
        dataType: 'json',
        success: function (data) {
          data.results.forEach (function (user, index) {
            // DOM削除
            $('#js-profile-' + index).empty();
            var dom = ''
            // 顔写真とテキスト枠
            + '\n<img class="profile__photo" src="' + user.picture.large + '" alt="">'
            + '\n<div class="profile__description" id="js-description-' + index + '">'
            // テキスト部
            // 名前
            + '\n<p class="profile__text js-loading js-text">' + user.name.title + ' ' + user.name.first + ' ' + user.name.last + '</p>'
            // 居住地
            + '\n<p class="profile__text-local js-text-local">' + user.location.state + ' state, ' + user.location.city + ', ' + user.location.street + ', ' + user.location.postcode + ', ' + '</p>'
            // 誕生日
            + '\n<p class="profile__text-birth js-text-birth">' + user.dob.date + ' , age ' + user.dob.age + ',' + '</p>'
            // DIVタグ閉じ
            + '\n</div>';
            $('#js-profile-' + index).append(dom);
          });
        },
      })
        .done(function () {
          $('.profile__description').removeClass('loading-more-reverse').addClass('loading-more-done-reverse');
          $('.profile__photo').removeClass('loading-more-photo-reverse').addClass('loading-more-done-reverse-photo');
        });
      return false;
    }});
});
